# Documentations publiques du Health Data Hub
<!-- SPDX-License-Identifier: MPL-2.0 -->

Bienvenue sur les documentations publiques du Health Data Hub.

Ce site héberge la [documentation collaborative du SNDS](/snds/).

Mais aussi une documentation sur [la standardisation au format OMOP](/omop/)

Cette documentation est en construction, via [ce dépôt GitLab](https://gitlab.com/healthdatahub/documentation-snds).

::: danger Attention
Internet Explorer cause des erreurs de navigation sur ce site, et sur GitLab. 

Nous conseillons d'utiliser un autre navigateur, car Microsoft a arrêté le développement d'Internet Explorer depuis quelques années.
:::
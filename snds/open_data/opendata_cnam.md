# Open Data de la CNAM
<!-- SPDX-License-Identifier: MPL-2.0 -->

Régulièrement, la [CNAM](../glossaire/Cnam.md) publie dans sa rubrique « Statistiques et Publications » des données relatives aux dépenses d'assurance maladie, à la consommation de soins et à l'activité des professionnels de santé, ainsi que les derniers rapports, analyses et études statistiques ou médico-économiques.

Ces séries statistiques de la CNAM, produites par la Direction de la Stratégie, des Etudes et des Statistiques (DSES) et issues notamment du Système National d’Information Inter Régimes de l’Assurance Maladie (SNIIRAM), ont été labellisées par l'Autorité de la statistique publique (arrêté du 14 avril 2015 sous le n° NOR FCPO1508846V).

## Données en open data

En complément de la rubrique « [statistiques et publications](https://www.ameli.fr/l-assurance-maladie/statistiques-et-publications/index.php) » d’ameli.fr, l’Assurance Maladie enrichit son offre de [données en open data](http://open-data-assurance-maladie.ameli.fr/index.php) en mettant à disposition des données de cadrage et des bases brutes extraites du [SNIIRAM](https://www.ameli.fr/l-assurance-maladie/statistiques-et-publications/sniiram/finalites-du-sniiram.php).Ces données sont plus particulièrement destinées à des réutilisations nécessitant une exploration détaillée et des retraitements.


LA CNAM enrichit son offre de données en open data en mettant à disposition des données de cadrage et des bases brutes extraites du SNIIRAM. Ces données sont plus particulièrement destinées à des réutilisations, nécessitant une exploration détaillée et des retraitements. Elles contiennent d’ores et déjà :

- sur le thème des dépenses d’assurance maladie : la base complète inter-régimes [Open Damir](http://open-data-assurance-maladie.ameli.fr/depenses/index.php#Open_DAMIR) avec 55 variables et 6 axes d’analyse, les [données nationales sources de la statistique mensuelle](http://open-data-assurance-maladie.ameli.fr/depenses/index.php#tables_N) ([Dépenses par type de risque](https://www.ameli.fr/l-assurance-maladie/statistiques-et-publications/donnees-statistiques/depenses-d-assurance-maladie/depenses-par-type-de-risque/depenses-mensuelles-2017.php)) et les [données par Cpam](http://open-data-assurance-maladie.ameli.fr/depenses/index.php#tables_R).

- sur le thème du médicament :
    - les données [Open Medic](http://open-data-assurance-maladie.ameli.fr/medicaments/index.php#Open_Medic) présentant l’ensemble des prescriptions de médicaments délivrés en officine de ville, que le prescripteur soit libéral ou salarié (prescriptions hospitalières principalement). La ventilation est opérée notamment par niveau de classe anatomique et thérapeutique et par code CIP, par caractéristique du bénéficiaire et par spécialité du prescripteur. Y figurent le nombre de consommants, les montants remboursés et remboursables ainsi que le nombre de boîtes délivrées;
    - les données [Open STATINES](http://open-data-assurance-maladie.ameli.fr/medicaments/index.php#Open_STATINES) proposant un focus sur les prescriptions de statines délivrées en officine de ville;
    - les données [Open PHMEV](http://open-data-assurance-maladie.ameli.fr/medicaments/index.php#Open_PHMEV) présentant les prescriptions hospitalières délivrées en officine de ville, par établissement prescripteur (numéro, catégorie juridique…) et par caractéristique du bénéficiaire. Y figurent les montants remboursés et remboursables ainsi que le nombre de boîtes délivrées

- sur le thème de la biologie médicale : les données [Open Bio](http://open-data-assurance-maladie.ameli.fr/biologie/index.php) présentant par groupe physiopathologique ou par code détaillé de la nomenclature, par caractéristique du bénéficiaire et par spécialité du prescripteur : le nombre de bénéficiaires, les montants remboursés et remboursables ainsi que le nombre d’actes prodigués.

- sur le thème des dispositifs médicaux inscrits à la liste des produits et prestations : les données [Open LPP](http://open-data-assurance-maladie.ameli.fr/LPP/index.php) présentant, par Titre et sous chapitre de niveau 1 et 2, code affiné, par caractéristique du bénéficiaire et par spécialité du prescripteur : le nombre de bénéficiaires, les montants remboursés et remboursables ainsi que la quantité des dispositifs médicaux délivrés.

### Open SNDS

[Open SNDS](https://open-snds.has-sante.fr/) est une application web pour interroger rapidement certaines données agrégées du SNDS, publiées en open data par l'Assurance maladie.

On interroge les données en choisissant une base (Open Medic, Open Bio, éventuellement d'autres à venir), puis en filtrant sur un (ou plusieurs) codes, aux différents niveaux de hierarchie des nomenclatures. 

On visualise alors le nombre de bénéficiaires, le nombre de consommations, et les montants de remboursements. 
Ces informations sont déclinées selon plusieurs axes d'analyse : évolution annuelle depuis 2014, répartition selon les caractéristiques du bénéficiaire (sexe, classe d'âge, région), ou la spécialité du prescripteur.
Elles sont affichées via un graphique, et un tableau qu'il est possible de télécharger.

L'application Open SNDS est développée et hébergée par la Haute Autorité de Santé.

L'application est accessible à cette [adresse](https://open-snds.has-sante.fr/).

## Lien d'accès
[Portail](http://open-data-assurance-maladie.ameli.fr/index.php) de la CNAM

[Application Open SNDS](https://open-snds.has-sante.fr) de la HAS

# 2. Le SNDS en bref
<!-- SPDX-License-Identifier: MPL-2.0 -->

## 2.1. Notre système de santé
### 2.1.1 Présentation générale
En France, le système de santé  est composé de **3 grands secteurs d’activité** :
* Soins **réalisés en ville** :
Les professionnels de santé libéraux ou salariés exercent, soit individuellement dans des cabinets, soit en groupe ou de manière coordonnée en maison de santé ou en centre de santé. Les professionnels de santé peuvent être des médecins généralistes ou spécialistes, des chirurgiens-dentistes, des pharmaciens, des sage-femmes, des infirmiers, des rééducateurs, etc.

* Soins **réalisés en établissements de santé (ES)** :
Ces établissements (toutes activités confondues) sont répartis selon trois catégories de <u>statut juridique</u> :
  * Les **hôpitaux publics** : centres hospitaliers régionaux (CHR), centre hospitaliers universitaires (CHU), centres hospitaliers (CH), etc. correspond à **61,5%** des lits en 2019 (cf. [Fiche 01 – Les grandes catégories d’établissements de santé](https://drees.solidarites-sante.gouv.fr/publications/panoramas-de-la-drees/les-etablissements-de-sante-edition-2021)),
  * Les **établissements privés d’intérêt collectif**, notamment les centres de lutte contre le cancer correspond à **14,5%** des lits en 2019(cf. [Fiche 01 – Les grandes catégories d’établissements de santé](https://drees.solidarites-sante.gouv.fr/publications/panoramas-de-la-drees/les-etablissements-de-sante-edition-2021)),
  * Les **cliniques privées à but lucratif** correspond à **24,0%** des lits en 2019 (cf. [Fiche 01 – Les grandes catégories d’établissements de santé](https://drees.solidarites-sante.gouv.fr/publications/panoramas-de-la-drees/les-etablissements-de-sante-edition-2021)).

Historiquement, en termes de **financement**, ces établissements étaient répartis en deux catégories :
- Les établissements antérieurement sous dotation globale de financement ([**ex-DG**](../../glossaire/ex-DG.md)) : hôpitaux publics et la très grande majorité des établissements privés d’intérêt collectif,
- Les établissements antérieurement sous objectif quantifié national ([**ex-OQN**](../../glossaire/ex-OQN.md)) : cliniques privées à but lucratif et de rares établissements privés d’intérêt collectif.  

Les établissements de santé peuvent être autorisés à exercer une ou plusieurs activités parmi ces 4 champs :
* **MCO** : Soins dits de courtes durées permettant de prendre en charge la phase aiguë de la maladie en Médecine ou Chirurgie ou Obstétrique ou Odontologie ([MCO](../../glossaire/MCO.md)). La médecine ou la chirurgie ambulatoire (hospitalisation < 12 h sans hébergement de nuit) fait partie de ces soins.
* **Psychiatrie** : Recueil d’information médicalisé pour la Psychiatrie ([RIM-P](../../glossaire/RIM-P.md) ou RIM-PSY). Soins de courtes, moyennes ou longues durées dans la prise en charge médicale concernant le domaine de la psychiatrie.
* **SSR** : Soins de Suites et de Réadaptation ([SSR](../../glossaire/SSR.md)) ou moyens séjours, prenant en charge la rééducation ou la réadaptation des patients (à partir du 12 janvier 2022, ils sont devenus  les Soins Médicaux et de Réadaptation - SMR).
* **HAD** : Hospitalisation à Domicile ([HAD](../../glossaire/HAD.md)) correspond à une hospitalisation complète où les soins sont réalisés à domicile pour des soins non réalisables en ville.

Il existe également des **USLD, devenues ES-SLD** : Unités de Soins de Longue Durée ou Établissement de Santé - Soins Longue Durée. Ce sont des structures d’hébergement et de soins au sein d’établissements de santé qui accueillent majoritairement des personnes âgées de plus de 60 ans. Les moyens médicaux qui y sont mis en œuvre sont plus importants que dans les EHPAD (établissements d'hébergement pour personnes âgées dépendantes). Ce champ est le seul à ne pas faire l’objet d’une collecte de données dans le cadre du Programme de Médicalisation des Systèmes d’Information (PMSI) (cf. section 2.3).

Certains établissements de santé autorisés en [MCO](../../glossaire/MCO.md) ont également des activités de médecine d’urgence avec des services d’urgence, des Services d’Aide Médicale Urgente (SAMU) ou des Services Mobiles d’Urgence et de Réanimation (SMUR).

* Soins *réalisés en établissements ou services médico-sociaux (ESMS)* :
Ils apportent un accompagnement et une prise en charge des personnes « fragiles », en situation de précarité, d’exclusion, de handicap ou de dépendances. 
Les **établissements médico-sociaux** regroupent notamment :
* Des Établissements d’Hébergement pour Personnes Agées Dépendantes (EHPAD)
* Des structures pour personnes handicapées : Foyers d’Accueil Médicalisé (FAM), Maisons d’Accueil Spécialisées (MAS), Instituts Thérapeutiques, Educatifs et Pédagogiques (ITEP), …
* Des structures pour les personnes en état de dépendance : Centres de Soins, d’Accompagnement et de Prévention en Addictologie (CSAPA), Centres d’Accueil et d’Accompagnement à la Réduction des risques pour Usagers de Drogues (CAARUD)…

Les **services médico-sociaux** sont très diversifiés ; ils comprennent par exemple des services d’aide et de soins à domicile : les services de soins infirmiers à domicile (SSIAD) qui permettent la surveillance à domicile ainsi que des soins d’hygiène et paramédicaux. A bien différencier de l’hospitalisation à domicile où les patients sont hospitalisés.

* Enfin, il est important de mentionner que certains soins peuvent être réalisés via la *télémédecine* : Elle comprend entre autres la téléconsultation, la téléexpertise ou la télésurveillance et permet de réaliser les pratiques médicales à distance, facilitées par les nouvelles technologies de l’information et de la communication.

### 2.1.2 Le système de remboursement des soins et les modes de financement des établissements de santé
En France, la sécurité sociale existe depuis 1945 (ordonnances du 4 et 19 octobre 1945) et a pour mission de protéger les Français contre tous les risques de la vie. Elle est subdivisée en 3 grands régimes (2 grands régimes depuis le 1er janvier 2018) et des régimes spéciaux, couvrant chacun une ou plusieurs catégories socioprofessionnelles spécifiques et se caractérisant par des modalités de gestion et de prise en charge différentes.

Tableau 1. Les différents régimes de la sécurité sociale

| Régimes                                                                       | Bénéficiaires                                                                                                                                                                                                               | % des bénéficiaires en France en 2016 |
|-------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------|
| **Régime Général (RG) + Sections Locales Mutualistes (SLM)**                      | Prise en charge de la majorité de la population, notamment la majorité des travailleurs salariés ainsi que toute personne bénéficiant de droit au titre de la résidence en France (la Protection Universelle Maladie, PUMa) | RG : 76% SLM : 11%                    |
| **Régime Social des Indépendants (RSI) puis Sécurité Sociale Indépendants (SSI)** | Prise en charge des travailleurs indépendants --> Sécurité Sociale des Indépendants (SSI) créé en 2018 avec une intégration progressive au régime général jusqu’en 2020                                                     | RSI : 5%                              |
| **Mutualité Sociale Agricole (MSA)**                                              | Prise en charge des salariés et exploitants agricoles                                                                                                                                                                       | MSA : 6%                              |
| **Autres régimes spéciaux (au nombre de 27 en 2021)**                             | Régime spécifique des marins, des mines, SNCF, RATP, etc.                                                                                                                                                                   | Régimes spéciaux : 2%                 |


Source : Tuppin P, et al. Value of a national administrative database to guide public decisions: From the système national d’information interrégimes de l’Assurance Maladie (SNIIRAM) to the système national des données de santé (SNDS) in France. Rev DÉpidémiologie Santé Publique. 2017 Oct;65:S149–67
La sécurité sociale se compose de 5 branches :
* La branche famille,
* La branche retraite,
* La branche recouvrement,
* La branche maladie,
* La branche AT/MP : Accidents du Travail - Maladies Professionnelles.

Les deux dernières branches (maladie et AT /MP) sont gérées par les assurances maladies, ce qui permet de retrouver les informations de santé dans les données du SNDS.

#### 2.1.2.1 Définition des bénéficiaires / ayant-droit
Chaque personne en France possède un identifiant unique appelé Numéro d’Identification au Répertoire national d'identification des personnes physiques ([NIR](../../glossaire/NIR.md)), attribué par l’Insee à chaque personne à la naissance et qui restera identique tout au long de sa vie. Le numéro de sécurité social est composé du [NIR](../../glossaire/NIR.md) et d'une clé de contrôle à 2 chiffres. Les personnes nées à l'étranger mais travaillant en France ou y habitant de façon stable et régulière peuvent également obtenir un [NIR](../../glossaire/NIR.md). 

Quand on parle de bénéficiaires assurance maladie, il faut différencier plusieurs termes :
* **Bénéficiaire des soins** : personne bénéficiant des soins, qu’elle soit assurée ou ayant-droit.
* **L’assuré** ou **ouvreur de droit** : personne qui est assurée à un régime de sécurité sociale. Le remboursement sera réalisé sous le nom et le [NIR](../../glossaire/NIR.md) de cette personne.
* **L’ayant-droit** : personne qui n’a pas de droits ouverts sous son nom, dont les soins sont remboursés sous le nom et le [NIR](../../glossaire/NIR.md) d’une autre personne (son ou ses ouvreur(s) de droit).
Par exemple, un enfant de moins de 18 ans ayant une consommation de soins sera défini comme ayant-droit d'un ou de plusieurs ouvreurs de droit (généralement les parents). A 18 ans, il deviendra lui-même son propre ouvreur de droit.

Informations complémentaires :

* Une personne ne peut ouvrir ses droits à un régime de sécurité sociale qu’à partir de 16 ans.
* Depuis le 1er janvier 2016, les ayants-droits majeurs résidant en France deviennent automatiquement ouvreur de droit avec la mise en œuvre de la Protection Universelle Maladie ([PUMa](../../glossaire/PUMa.md), anciennement Couverture Maladie Universelle - CMU. Les personnes de plus de 16 ans ou les étudiants dans l’enseignement supérieur peuvent également en faire la demande.

A noter : Dans le cadre de la stratégie ma santé 2022, un identifiant unique a été créé, l'Identifiant National de Santé (INS). L'identité INS permet ainsi de référencer toutes les données de santé de manière sécurisée. Sa mise en œuvre a débuté au 1er janvier 2021. Les conditions et modalités d'utilisation du [NIR](../../glossaire/NIR.md) comme INS sont précisées aux articles R. 1111-8-1 à R. 1111- 8-7 du Code de la Santé Publique ([CSP](../../glossaire/CSP.md)).

#### 2.1.2.2 Système de remboursement des bénéficiaires en France
* Les régimes d’Assurance Maladie Obligatoire (AMO) de la sécurité sociale :
L’adhésion et la cotisation à un régime d’assurance maladie est obligatoire pour chaque individu. Ces régimes reposent sur un principe de solidarité où chaque individu contribue selon ses revenus et a un accès aux soins définis selon ses besoins. 
Ils proposent des remboursements de prestations et des aides financières :
  * Remboursements des soins de santé : consultations, hospitalisations, médicaments, etc.
Ces soins sont remboursés selon un pourcentage variable (pouvant aller jusqu’à 100%) par l’assurance maladie.
  * Indemnités journalières : arrêt de travail, congés suite à une naissance ou une adoption, etc.
  * Pensions, allocations et rentes : pension d’invalidité, rente incapacité, etc.

* Les régimes complémentaires :
Ces régimes (mutuelles, assurances privées, etc.) peuvent prendre en charge ce qui n’est pas remboursé par l’assurance maladie (ticket modérateur, franchises, forfaits hospitaliers, etc.), les dépassements d’honoraire, ou bien encore des soins non remboursés par l’assurance maladie. La prise en charge partielle ou totale est dépendante du contrat souscrit avec le régime complémentaire.

* Les financeurs publics : l’Etat
L'Etat finance notamment le système de Complémentaire Santé Solidaire (C2S) et l’Aide Médicale de l'Etat ([AME](../../glossaire/AME.md)).
  * Avant le 1er novembre 2019 *:
    * CMU complémentaire ([CMU-C](../../glossaire/CMUC.md)) : remboursement total des frais de santé pour les personnes ayant des revenus inférieurs à un seuil défini.
    * Aide à la Complémentaire Santé ([ACS](../../glossaire/ACS.md)) : remboursement des frais de santé à un coût minimal pour les personnes ayant des revenus légèrement supérieurs aux bénéficiaires de la [CMU-C](../../glossaire/CMUC.md) mais toujours inférieurs à un seuil dépendant du seuil de pauvreté.
  * Après le 1er novembre 2019 *: 
    * CMU-C](../../glossaire/CMUC.md) et [ACS](../../glossaire/ACS.md) remplacées par la Complémentaire Santé Solidaire (C2S).
Pour les personnes les plus défavorisées (revenus mensuels inférieurs à un seuil défini dépendant du seuil de pauvreté), des aides pour une complémentaire santé sont possibles pour avoir accès à un remboursement total des frais de santé.
    * Aide Médicale de l’Etat ([AME](../../glossaire/AME.md)) :
Permet un accès aux soins des personnes en situation irrégulière selon la réglementation sur le séjour en France et est attribuée sous conditions de résidence et de ressources.

Les demandes d’inscription à la C2S et l’[AME](../../glossaire/AME.md) sont à réaliser auprès de l’assurance maladie.

* Les ménages : reste à charge

En fonction des règles de remboursement de l’assurance maladie et du régime complémentaire, des dépenses peuvent demeurer à la charge du bénéficiaire : ticket modérateur, forfait hospitalier, participation forfaitaire de 1 euro, médicaments non-inscrits au remboursement, etc.


Chaque année, la Loi de Financement de la Sécurité Sociale ([LFSS](../../glossaire/LFSS.md)) est votée par les parlementaires et fixe l’Objectif National des Dépenses d’Assurance Maladie ([ONDAM](../../glossaire/ONDAM.md)) créé par l’ordonnance du 24 avril 1996. Cet objectif définit les prévisions de recettes et les objectifs de dépenses de la sécurité sociale, répartis en différents compartiments :
* Soins de ville,
* Etablissements de santé (publics et privés),
* Etablissements et services médico-sociaux,
* Autres prises en charge (dont Fonds d'Intervention Régionale (FIR)).

#### 2.1.2.3 Financement des établissements de santé
Comme inscrit dans la [LFSS](../../glossaire/LFSS.md) de 2004, les établissements de santé [MCO](../../glossaire/MCO.md) et [HAD](../../glossaire/HAD.md) ([ex-DG](../../glossaire/ex-DG.html et [ex-OQN](../../glossaire/ex-OQN.md)) sont désormais financés selon le même modèle, la Tarification A l’Activité ([T2A](../../glossaire/T2A.md)). Ce modèle repose sur un principe de mesure de la nature et du volume des activités réalisées. La facturation des séjours des établissements de santé (ES) et des actes externes des ES [ex-DG](../../glossaire/ex-DG.md) s’appuie sur la production PMSI MCO et PMSI HAD.

Tableau 2. Ventilation de l’ONDAM concernant les établissements de santé
| MCO | HAD |SSR  |  RIP |
|---|---|---|---|
|**ODMCO, Objectif de Dépense de MCO divisé en trois enveloppes financières** : 1. La part tarif : prestations des séjours hospitaliers (Groupe Homogène de Séjours : [GHS](https://documentation-snds.health-data-hub.fr/snds/glossaire/GHS.html)) ainsi que des Actes et Consultations Externes (ACE) des ES [ex-DG](https://documentation-snds.health-data-hub.fr/snds/glossaire/ex-DG.html). 2. La liste en sus : dépenses des spécialités (médicaments et dispositifs médicaux implantables) facturables en sus des hospitalisations. Ces deux premiers points sont financés selon l’activité ([T2A](https://documentation-snds.health-data-hub.fr/snds/glossaire/T2A.html)). 3. Les forfaits annuels : finance en partie les activités d’urgence et coordination, prélèvements d’organes, greffes d’organes|**ODMCO, Objectif de Dépense de MCO**. 1. La part tarif : journées d’hospitalisation à domicile (Groupe Homogène de Tarifs : GHT). 2. Les médicaments sur la liste en sus. Ces deux points sont financés selon l’activité ([T2A](https://documentation-snds.health-data-hub.fr/snds/glossaire/T2A.html))| **Jusqu'en 2016** : ODAM, Objectif de Dépenses de l’Assurance Maladie : 1. Établissements publics ou privés à but non lucratif : DAF, Dotation Annuelle de Financement ; financement alloué par la région (ARS) en dotation. 2. Cliniques privés ou certains établissements privés à but non lucratif : OQN, Objectif Quantifié National; financement alloué à l’activité. |**Jusqu'en 2021** : ODAM, Objectif des Dépenses de l’Assurance Maladie. 1. Établissements publics ou privés à but non lucratif : DAF, Dotation Annuelle de Financement; financement alloué par la région (ARS) en dotation. 2. Cliniques privés ou certains établissements privés à but non lucratif : OQN, Objectif Quantifié National; financement alloué à l’activité.|
|**MIGAC, Mission d’Intérêt Général et d’Aide à la Contractualisation** : Financement d’activités non tarifées à l’activité (enseignement, recherche, SAMU/SMUR …) à l’aide de dotations| -  |**A partir de 2017** : ODSSR, Objectif de Dépense de SSR. Le nouveau modèle de financement des établissements SSR est mixte et repose sur des recettes directement liées à l’activité assorties de compartiments complémentaires. Il est composé :1. D’une part activité : permet de prendre en compte l’activité réalisée sur l’année en cours. 2. D’une part forfaitaire : base de ressources minimales permettant de mettre en œuvre des stratégies pluriannuelles et de lisser les aléas de revenus liés à l’activité. 3. Des compartiments complémentaires : « Plateaux techniques spécialisés », « molécules onéreuses », « Missions d’intérêt général (MIG) » etc.Le déploiement progressif de cette réforme est prévu de début 2017 à fin 2022.|**A partir de 2022** : Les nouveaux modes de financement comprendront une dotation à la file active, une dotation à la qualité du codage et une dotation populationnelle.|

La rémunération à l’acte ([NGAP](../../glossaire/NGAP.md), [CCAM](../../glossaire/CCAM.md)) des professionnels de santé libéraux intervenant lors des séjours dans les ES privés [ex-OQN](../../glossaire/ex-OQN.md) n’est pas modifiée. 

## 2.2. Sources des données
### 2.2.1 Le SNDS
Le Système National des Données de Santé (SNDS) a été instauré par la loi du 26 janvier 2016 de modernisation du système de santé et dont les modalités de mise en œuvre ont été approfondies par la loi du 24 juillet 2019 puis le décret du 29 juin 2021. Son objectif est de développer l’usage des données de santé, d’analyser et d'améliorer la santé de la population en croisant plusieurs sources de données (Figure 1).

La base principale du SNDS comprend 3 sources principales :
* Le Système National d’Information Inter-Régime de l’Assurance Maladie ([**SNIIRAM**](../../glossaire/SNIIRAM.md)): données exhaustives concernant les **dépenses de l’Assurance Maladie** et gérées par la [**Cnam**](../../glossaire/Cnam.md),
* Le Programme de Médicalisation des Systèmes d’Information ([**PMSI**](../../glossaire/PMSI.md)) : données concernant les établissements de santé et gérées par l’[**ATIH**](../../glossaire/ATIH.md),
* Les **causes médicales de décès :** données issues des certificats de décès, gérées par le [**CépiDc**](../../glossaire/CepiDC.md) de l’[**Inserm**](../../glossaire/inserm.md).

Deux nouvelles sources, créées lors de l'épidémie de COVID-19 pour suivre différents indicateurs, vont être implémentées au SNDS suivant l'arrêté du 12 mai 2022 : 
* La base **vaccin covid, VAC-SI** : le Système d’Information Vaccin COVID contient les informations concernant les vaccinations contre la COVID-19.
* La base **SI-DEP** : le Système d’Information national de Dépistage Populationnel de la COVID-19 contient les informations sur les résultats des tests de dépistage de la COVID-19.

Sur le portail SNDS géré par la [Cnam](../../glossaire/Cnam.md), les données sont restituées sous trois systèmes :
* 15 bases de **données agrégées thématiques** : les **datamarts** sur différents domaines spécifiques (soins libéraux, biologie médicale, pharmacie, dispositifs médicaux, etc.)
* L’Echantillon de Bénéficiaires du SNDS (ESND), **données individuelles échantillonnées** : échantillon issu d'un tirage aléatoire simple au 2/100ème des bénéficiaires consommants de la base principale du [SNIIRAM](../../glossaire/SNIIRAM.md). Il est représentatif par âge, sexe,  grands régimes d'affiliation, région et département de résidence des bénéficiaires. Cet échantillon possédant la même structure que les données individuelles exhaustives, permet la réalisation d’études longitudinales à l'aide de données de villes (consommations de soins) et hospitalières.
* Les **données individuelles exhaustives** : ce système comprend les données de soins de ville, des séjours hospitaliers et des causes médicales de décès de la totalité des bénéficiaires en France, permettant de réaliser des études sur près de 99% de la population française.

Ce guide sera principalement axé sur l’exploitation des données individuelles exhaustives, mais les informations contenues dans ce document pourront être facilement applicables aux données des autres datamarts (et notamment à l'ESND).

Figure 1. Sources d'alimentation du SNDS
![image](../../files/HDH/202212_HDH_alimentation-snds_MPL-2.0.png)

### 2.2.2 Le DCIR

L'entrepôt de données géré par la [Cnam](../../glossaire/Cnam.md) s'appelle le Système National d'Information Inter Régimes de l'Assurance Maladie ([SNIIRAM](../../glossaire/SNIIRAM.md)). Il a été créé par la [LFSS](../../glossaire/LFSS.md) du 23 décembre 1998, devant la nécessité de **suivre les dépenses d’assurance maladie**, tous régimes confondus suite à la création de l’[ONDAM](../../glossaire/ONDAM.md).
Un des produits de restitution du [SNIIRAM](../../glossaire/SNIIRAM.md) est le Datamart de Consommation Inter-Régime ([DCIR](../../glossaire/DCIR.md)) contenant les données de remboursement individuelles exhaustives des bénéficiaires. 
**Nous nous intéresserons dans la suite de ce document au [DCIR](../../glossaire/DCIR.md).**
Le DCIR contient (cf. description détaillée dans la section 2.3.1) :
* **Les données des remboursements** des soins du **secteur libéral** pour l’**ensemble des régimes d’assurance maladie** : code prestation, date de soins et montant remboursé par les régimes d’assurance maladie, 
* Les données de remboursement des soins et prestations facturés par les centres de santé et par les établissements de santé [ex-OQN](../../glossaire/ex-OQN.md),
* Des données de remboursement des séjours [MCO](../../glossaire/MCO.md), des actes et consultations externes pour les établissements de santé [ex-DG](../../glossaire/ex-DG.md) qui ont basculé en facturation individuelle directe ([FIDES](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/performance-des-etablissements-de-sante/simphonie/fides)),
* Des informations concernant le bénéficiaire : caractéristiques socio-démographiques, C2S (anciennement [CMU-C](../../glossaire/CMUC.md) et [ACS](../../glossaire/ACS.md)), Affection de Longue Durée ([ALD](../../glossaire/ALD.md)), etc.
* Des informations concernant les professionnels de santé libéraux : spécialité, mode d’exercice, etc.

Note : Le but initial et principal de cette base est de suivre les prestations remboursées par l'assurance maladie. Les informations présentes sont de natures médico-administratives. Cela est à prendre en compte dans la réalisation d’étude de recherche sur cette base, qui est exhaustive sur les consommations de soins remboursés mais qui ne contient ni données cliniques (poids, taille, tension artérielle, etc.), ni résultats d’examen ou d’actes (imagerie médicale, biologie médicale, compte-rendu opératoire, etc.), ni données socio-démographiques détaillées sur les bénéficiaires (catégorie socio-professionnelle, niveau de revenus, etc.). 

Il existe un DCIR Simplifié, appelé DCIRS, qui est une version simplifiée du DCIR avec des données recalculées pour faciliter l'exploitation des données. L'historique du DCIRS n'est cependant disponible que pour les années 2016 à 2019.

### 2.2.3 PMSI
Le [PMSI](../../glossaire/PMSI.md) est la base contenant les informations médico-administratives sur l’activité hospitalière, gérée par l’[ATIH](../../glossaire/ATIH.md). La finalité du [PMSI](../../glossaire/PMSI.md) est l’analyse médico-économique et le pilotage de l’activité des établissements de santé (ES) ainsi que le financement et allocation budgétaire.

4 champs d'activité sanitaire font l’objet d’une production [PMSI](../../glossaire/PMSI.md) :
* [MCO](../../glossaire/MCO.md) (a concerné 12,4 millions de bénéficiaires en 2019) [cf. [ATIH. Analyse de l’activité hospitalière 2019]( https://www.atih.sante.fr/analyse-de-l-activite-hospitaliere-2019/)],
* [HAD](../../glossaire/HAD.md) (a concerné 128 000 bénéficiaires en 2019) [cf. [ATIH. Analyse de l’activité hospitalière 2019]( https://www.atih.sante.fr/analyse-de-l-activite-hospitaliere-2019/)],
* [SSR](../../glossaire/SSR.md) (a concerné 1 million de bénéficiaires en 2019) [cf. [ATIH. Analyse de l’activité hospitalière 2019]( https://www.atih.sante.fr/analyse-de-l-activite-hospitaliere-2019/)],
* Psychiatrie ou [RIM-P](../../glossaire/RIM-P.md) (a concerné 419 000 bénéficiaires en 2019) [cf. [ATIH. Analyse de l’activité hospitalière 2019]( https://www.atih.sante.fr/analyse-de-l-activite-hospitaliere-2019/)].

::: tip Focus sur le PMSI et MCO

Les informations administratives et médicales des séjours hospitaliers recueillies sont utilisées pour le financement des établissements de santé, avec la [T2A](../../glossaire/T2A.md), et pour l’organisation de l’offre de soins.

Pour chaque séjour hospitalier, les informations médicales et administratives sont colligées dans un Résumé de Séjour Anonymisé ([RSA](../../glossaire/RSA.md)) :
* **Informations administratives** :
  * Sur l’établissement : Identification de l’établissement,
  * Sur le séjour : Durée, mode d’entrée et de sortie, unités médicales,
  * Sur le patient : Age, sexe, code géographique de résidence (quasi équivalent au code postal).
* **Informations médicales** :
  * Pathologie ou motif de recours aux soins ayant motivé le séjour codé avec un diagnostic principal ([DP](../../glossaire/DP.md)) et éventuellement complété d’un diagnostic relié (DR), codés grâce à la Classification Internationale des Maladies, 10ème révision([CIM](../../glossaire/CIM.md)-10).
  * Des diagnostics associés significatifs (DAS) constituant soit une affection supplémentaire présente à l’admission et qui majore l’effort de prise en charge hospitalier, soit une complication survenue au cours du séjour.
  * Actes médico-techniques réalisés durant le séjour, codés grâce à la Classification Commune des Actes Médicaux ([CCAM](../../glossaire/CCAM.md)).
A partir de ces codes, le séjour est classé par un algorithme de groupage développé par l’[ATIH](../../glossaire/ATIH.md) dans un Groupe Homogène de Malades ([GHM](../../glossaire/GHM.md)), ensuite valorisé via un Groupe Homogène de Séjours ([GHS](../../glossaire/GHS.md)).

Le PMSI MCO (mais aussi SSR) contient également des informations sur les **actes et consultations externes (ACE)** des ES [ex-DG](../../glossaire/ex-DG.md). L’’activité externe des professionnels libéraux en établissement [ex-OQN](../../glossaire/ex-OQN.md) est considérée comme du soin de ville libéral et facturée comme tel (i.e. ces actes ne sont pas dans le [PMSI](../../glossaire/PMSI.md) mais dans le DCIR). L’activité externe du PMSI MCO est définie comme l’ensemble des admissions aux urgences sans hospitalisation et les consultations et actes externes des établissements de santé [ex-DG](../../glossaire/ex-DG.md).

A noter que le RIM-Psychiatrie contient également les actes et consultations externes en établissement [ex-DG](../../glossaire/ex-DG.md), mais sous un format différent de celui appliqué en MCO/SSR.

Le recueil des informations [PMSI](../../glossaire/PMSI.md) couvre l’ensemble des hospitalisations dans les établissements de santé publics et privés en MCO, SSR, Psychiatrie ou HAD, indépendamment du régime d’assurance maladie du bénéficiaire.

Il est important de souligner que des données [PMSI](../../glossaire/PMSI.md) sont également produites pour les patients non bénéficiaires de l'AMO (ex : patients étrangers, [AME](../../glossaire/AME.md), etc.) ou pour des activités hospitalières non remboursées (ex : chirurgie esthétique).
:::

### 2.2.4 Causes médicales de décès
La base des causes médicales de décès, gérée par le [CépiDc](../../glossaire/CepiDC.md) (Centre d’épidémiologie sur les causes médicales de Décès, laboratoire [Inserm](../../glossaire/inserm.md) en collaboration avec l'Insee), a été créée en 1968 afin de pouvoir élaborer annuellement les statistiques nationales des causes de décès à partir des certificats de décès. Cette mission est inscrite dans la loi française (article L.2223-42 du CGCT). Elle permet d’identifier les priorités de santé publique, notamment pour pouvoir orienter la recherche sur ces sujets.

Les informations présentes sur le certificat de décès et disponibles dans la base SNDS sont : 
* Les différentes causes médicales à l’origine du décès avec un ordre d’importance,
* Les états morbides qui ont contribué au décès.

### 2.2.5 Autres produits
#### 2.2.5.1 Nomenclatures de santé pour le codage et la facturation

Les nomenclatures utilisées dans le cadre du SNDS sont gérées et mises à jour régulièrement par différentes structures :
* Gérées et maintenues par la [Cnam](../../glossaire/Cnam.md) et l'[ATIH](../../glossaire/ATIH.md) :
  * Actes médico-techniques : Classification Commune des Actes Médicaux ([CCAM](../../glossaire/CCAM.md))
[Cnam](https://www.ameli.fr/accueil-de-la-ccam/index.php) : version utilisée pour la facturation (DCIR)
[ATIH](https://www.atih.sante.fr/ccam-descriptive-usage-pmsi-2022) : version utilisée pour la description et la facturation (PMSI)
* Médicaments : 
  * Code Identifiant de Présentation ([CIP](../../glossaire/CIP.md)) : médicaments remboursables aux assurés sociaux et agréés aux collectivités (DCIR).
  * Unité Commune de Dispensation ([UCD](../../glossaire/UCD.md)) : médicaments rétrocédés ou facturables en sus des [GHS](../../glossaire/GHS.md), des [GHPC](../../glossaire/GHPC.md) ou des [GME](../../glossaire/GME.md) par les établissements de santé (PMSI et DCIR).
    * [CIP et UCD Cnam](http://www.codage.ext.cnamts.fr/codif/bdm_it/index_presentation.php?p_site=AMELI/)
    * [UCD ATIH](https://www.atih.sante.fr/unites-communes-de-dispensation-prises-en-charge-en-sus)

* Dispositifs médicaux implantables (DMI) : 
  * Liste de DMI issue de la [LPP](../../glossaire/LPP.md)
[DMI ATIH](https://www.atih.sante.fr/dispositifs-medicaux-pris-en-charge-en-sus/) 

* Gérées et maintenues par la [Cnam](../../glossaire/Cnam.md) :
  * Actes de biologie : Nomenclature des Actes de Biologie Médicale ([NABM](../../glossaire/NABM.md))
[Cnam](http://www.codage.ext.cnamts.fr/codif/nabm/index_presentation.php?p_site=AMELI/)
  * Actes généraux des professionnels de santé (hors [CCAM](../../glossaire/CCAM.md) et hors [NABM](../../glossaire/NABM.md)) : 
[NGAP](../../glossaire/NGAP.md)
[Cnam](https://www.ameli.fr/medecin/exercice-liberal/remuneration/consultations-actes/nomenclatures-codage/ngap/) 
  * Liste des produits et prestations ([LPP](../../glossaire/LPP.md)) : [Cnam](http://www.codage.ext.cnamts.fr/codif/tips/index_presentation.php?p_site=AMELI)
  * Normes encadrant les échanges informatiques de données entre ES et assurance maladie(Norme B2-Noémie) : 
[Cnam](https://www.ameli.fr/assure/documentation-technique/norme-b2)

* Gérée et maintenue par l’[OMS](../../glossaire/OMS.md) :
  * Médicaments : classifications Anatomique, Thérapeutique et Chimique (ATC)
[OMS](https://www.whocc.no/atc_ddd_index/)

* Gérée et maintenue par l’[OMS](../../glossaire/OMS.md) et l'[ATIH](../../glossaire/ATIH.md) :
  * Maladies, signes, symptômes, circonstances sociales et causes externes de maladies ou de blessures : Classification Internationale des Maladies, 10ème version ([CIM](../../glossaire/CIM.md)-10), qui est modifiée et complétée par l'[ATIH](../../glossaire/ATIH.md) dans la version utilisée dans le PMSI
    * [OMS](https://icd.who.int/browse10/2019/en#/)
    * [ATIH](https://www.atih.sante.fr/cim-10-fr-2022-usage-pmsi)

#### 2.2.5.2 Référentiels

Les principaux référentiels du SNDS sont énumérés ici et les liens renvoient vers des fiches présentant leurs caractéristiques. 
* Référentiel des bénéficiaires **`IR_BEN_R`** : contient des informations socio-démographiques des bénéficiaires. Ce référentiel dispose d’une version actuelle (`IR_BEN_R`) portant sur les bénéficiaires ayant eu au moins une consommation depuis le 01/01/2013 et une version archivée (`IR_BEN_R_ARC`) portant sur les bénéficiaires ayant eu au moins une consommation avant le 01/01/2013. [Référentiel bénéficiaires](../../tables/.sources/BENEFICIAIRE/IR_BEN_R.md)

* Référentiel médicalisé des bénéficiaires **`IR_IMB_R`** : contient des informations médicalisées (exonération pour [ALD](../../glossaire/ALD.md), maladie professionnelle, etc.). [Référentiel médicalisé](../../tables/.sources/DCIR_DCIRS/IR_IMB_R.md)

* Référentiel des médicaments **`IR_PHA_R`** : permet d'avoir des informations sur les médicaments délivrés en officine (identification via les classifications ATC, [CIP](../../glossaire/CIP.md) et [UCD](../../glossaire/UCD.md), la composition, la date de mise sur le marché, le nom du laboratoire, etc.).

La mise à jour mensuelle à partir des parutions au Journal Officiel est effectuée par le « département d’Etudes sur les pathologies & les patients » de la [Cnam](../../glossaire/Cnam.md). Une description détaillée du référentiel est disponible dans le communiqué du 24/01/2012.
  * [Référentiel médicaments](../../fiches/medicament.md)

* Référentiel des actes de biologie **`IR_BIO_R`** : contient l’historique complet des modifications de tarifs des actes de biologie médicale (identification des actes via la classification [NABM](../../glossaire/NABM.md)).

* Référentiel d’établissements **`BE_IDE_R`** : permet d’avoir des informations concernant les établissements sanitaires et médico-sociaux grâce au numéro FINESS. Ce référentiel couvre les établissements de santé ([ex-DG](../../glossaire/ex-DG.md) et [ex-OQN](../../glossaire/ex-OQN.md)) et les ESMS mais également les centres de santé, PMI, les centres dentaires, …
[Référentiel d’établissements](../../fiches/ref_etab.md)

* Référentiel des bénéficiaires de soins en établissement ou service médico-social (ESMS) **`IR_ESM_R`** : permet l’identification de bénéficiaires pris en charge par un service ou résidant ou ayant résidé dans un ESMS (EHPAD, autres ESMS, etc.), selon différentes caractéristiques. A noter que les patients hospitalisés en ES-SLD sont également référencés dans ce référentiel.
[Référentiel bénéficiaires soins médico-sociaux](../../fiches/IR_ESM_R.md)

* Référentiel des Professionnels de Santé `DA_PRA_R` : contient des informations sur l’activité des Professionnels de Santé ([PS](../../glossaire/PS.md)) (mode d’exercice, différents cabinets, etc.). 
[Référentiel professionnels de santé](../../fiches/DA_PRA_R.md)

Seuls les référentiels `IR_PHA_R` et `IR_BIO_R` sont disponibles et téléchargeables au format CSV dans le [dictionnaire interactif](https://health-data-hub.shinyapps.io/dico-snds/) (onglet "recherche dans les nomenclatures").

#### 2.2.5.3 Autres sources

**Catalogue du SNDS**
Des bases de données seront accessibles via la plateforme du Health Data Hub ([HDH](../../glossaire/HDH.md)), en plus de la base principale du SNDS qui inclut Vaccin-covid et SI-DEP. Ces bases de données seront listées dans le [catalogue du SNDS](https://www.health-data-hub.fr/catalogue-de-donnees).
Suivant l'arrêté du 12 mai 2022, les bases suivantes feront parties du catalogue de données disponibles :

* La base de données relative au dépistage du cancer du sein des départements du Gard et de la Lozère, dénommée « **base E-SIS** »
* La base de données relative à l'enquête statistique sur l'état de santé et les conditions de vie en lien avec l'épidémie due au coronavirus, dénommée « **EpiCov** »
* La base de données relative à la banque nationale des maladies rares centralisant les dossiers patients informatisés créés par les centres experts, dénommée « **BNDMR** »
* La base de données relative aux données de surveillance de 33 maladies à déclaration obligatoire permettant de prévenir les risques d'épidémie, dénommée « **Maladies déclaration obligatoire** »
* La base de données relative à l'accès précoce de 1 402 patients atteints de cancer bronchique à petites cellules traités par TECENTRIQ en association au carboplatine et à l'étoposide en première ligne de traitement de stade étendu et présentant un score ECOG de 0 ou 1, dénommée « **ATU CBPC atezolizumab** »
* La base de données relative aux patients atteints d'hépatite B ou C, dénommée « **HEPATHER** » 
* La base de données relative à l'exploitation des données de passages aux urgences, dénommée « **OSCOUR** »
* La base de données relative à l'entrepôt de données structurées issues des dossiers médicaux de patientes atteintes d'un cancer du sein métastatique prises en charge dans des Centres régionaux de lutte contre le cancer, dénommée « **ESME CSM** »
* La base de données relative à une étude sur l'histoire naturelle de la maladie d'Alzheimer, dénommée « **MEMENTO** »
* La base de données relative aux infarctus du myocarde pris en charge par les Services d'Aide Médicale Urgente (SAMU) et les Structures Mobiles d'Urgences et de Réanimation (SMUR) d'Ile-de-France, dénommée « **registre e-MUST** »

**Données relatives au handicap**

L’ajout de données provenant des Maisons Départementales des Personnes Handicapées ([MDPH](../../glossaire/MDPH.md)) au SNDS est en projet. Cela permettra d’observer des liens entre les accompagnements proposés par ces structures et le parcours de soins des bénéficiaires.

## 2.3 Synthèse des données disponibles
### 2.3.1 Le DCIR
![image](../../files/HDH/2022-12-12_HDH_DCIR_MLP-2.0.png)

### 2.3.2 PMSI MCO (séjours ou séances)
![image](../../files/HDH/2022-12-12_HDH_PMSI_MCO_MLP-2.0.png)

### 2.3.3 PMSI HAD
![image](../../files/HDH/2022-12-12_HDH_PMSI_HAD_MLP-2.0.png)

### 2.3.4 PMSI SSR (séjours)
![image](../../files/HDH/2022-12-12_HDH_PMSI_SSR_MLP-2.0.png)

Il est prévu une suppression du FPP dans le PMSI à partir de 2023 : le codage de la Manifestation Morbide Principale (MPP) et de l’Affection Etiologique (AE) est maintenu.
### 2.3.5 PMSI PSY (RIM-P) (séjours ou actes externes (EDGAR))
![image](../../files/HDH/2022-12-12_HDH_PMSI_PSY_MLP-2.0.png)

## 2.4. Avantages et limites de ces données
* **Avantages :**
  * Données exhaustives sur les hospitalisations et les consommations de soins remboursées.
  * Données exhaustives pour les montants remboursés par l'assurance maladie.
  * Le chaînage de plusieurs données permet de suivre le parcours de soins ville/hôpital de chaque individu.
  * La profondeur des données permet de créer des cohortes avec de longues périodes de suivi sans perdus de vue.
  * Données collectées de manière prospective et de manière identique : pas de biais de mémorisation, variables standardisées et préexistantes au recueil de données.
* **Limites :**
  * Peu de données cliniques, en particulier sur les soins et prestations réalisés en ville.
  * Aucun résultat d’examen (analyse biologique, imagerie, etc.).
  * Pas de données sur la consommation effective par les patients des médicaments (seulement sur la délivrance), ni sur l’utilisation effective des dispositifs médicaux (en ville).

  * L’utilisation du [PMSI](../../glossaire/PMSI.md) pour la facturation hospitalière génère des biais de production des données. Par exemple, le codage des diagnostics associés ([DA](../../glossaire/DA.md)) peut ne pas être exhaustif (il est même parfois limité aux pathologies inscrites à la liste des complications et morbidités associées ([CMA](../../glossaire/CMA.md)). Autre exemple, le codage des actes [CCAM](../../glossaire/CCAM.md) peut ne pas être exhaustif (certains actes non classants et à visée diagnostique peuvent ne pas être codés (radiographie de thorax, ponction lombaire, électrocardiogramme, etc.)).
  * Pas d’information sur la situation socio-économique du patient (niveau d’étude, chômage, etc.). Le FDep ne caractérise que très indirectement le patient car il caractérise sa commune de résidence ; il est à utiliser avec précaution pour les communes très peuplées et présentant une hétérogénéité populationnelle (ex : zones résidentielles favorisées limitrophes de quartiers politique de la ville (QPV)). 

## 2.5. Responsabilité individuelle en terme de sécurité des données de santé
Le SNDS contient des données de santé individuelles et à ce titre, le traitement de ces données est régi par plusieurs textes réglementaires afin de protéger les libertés et droits fondamentaux des personnes :
* La Loi Informatique et Libertés (loi no 78-17 du 6 janvier 1978),
* Règlement Européen sur la protection des données (RGPD) à compter du 25 mai 2018,
* Code de la Santé Publique.

Un [référentiel](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000034265125/) est mis en place afin de préciser les règles de sécurité afin notamment de préserver l’anonymat des bénéficiaires et des professionnels de santé ([PS](../../glossaire/PS.md)) en mettant en place notamment une pseudonymisation de l’identifiant du bénéficiaire et de l'identifiant du [PS](../../glossaire/PS.md).

Ainsi, pour être autorisées, toutes les études doivent poursuivre une finalité [d’intérêt public]( https://www.health-data-hub.fr/interet-public#:~:text=La%20garantie%20de%20normes%20%C3%A9lev%C3%A9es,finalit%C3https://www.health-data-hub.fr/interet-public) et vérifier les finalités propres au SNDS.

Ce règlement engage également la responsabilité individuelle de chaque utilisateur du SNDS. Concrètement, l’utilisateur est acteur de la protection de ces données en respectant  :

- Les conditions d’accès : suivre les formations pour être habilité à accéder aux données SNDS via le portail ;

- Les Conditions Générales d’Utilisation (CGU) : respecter les finalités d’utilisation autorisées et interdites du SNDS, ne pas chercher à ré-identifier des bénéficiaires, ne diffuser que des données anonymes et respecter le référentiel de sécurité.

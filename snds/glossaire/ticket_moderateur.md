# TM - Ticket modérateur
<!-- SPDX-License-Identifier: MPL-2.0 -->

Le ticket modérateur (TM) correspond à la part des frais de soins qui n’est pas remboursée par l’assurance maladie et reste donc à la charge du patient ou de son éventuel organisme complémentaire (mutuelle, couverture maladie universelle …). 
Elle est établie par décision de l’Union nationale des caisses d’assurance maladie (<link-previewer href="UNCAM.html" text="UNCAM" preview-title="UNCAM - Union nationale des caisses d’assurance maladie" preview-text="L'Union nationale des caisses d'assurance maladie (UNCAM) est une instance créée par la loi du 13 août 2004 relative à l’assurance maladie, pour regrouper les trois principaux régimes d'assurance maladie :" />) après avis de l’Union nationale des organismes complémentaires d’assurance maladie (<link-previewer href="UNOCAM.html" text="UNOCAM" preview-title="UNOCAM - Union nationale des organismes complémentaires d’assurance maladie" preview-text="Aucune définition détaillée n'existe pour l'instant dans le glossaire. Pour contribuer, référrez-vous au guide de contribution." />) dans les limites fixées, selon les prestations, par l’article R 322-1 du code de la sécurité sociale. 
Les décisions de l’UNCAM sont réputées approuvées sauf opposition motivée des ministres de la santé et de la sécurité sociale.

# Références

- [Page wikipedia](https://fr.wikipedia.org/wiki/Ticket_mod%C3%A9rateur)
- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/ticket-moderateur) sur le site internet du ministère

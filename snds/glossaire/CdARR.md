# CdARR - Catalogue des activités de rééducation-réadaptation
<!-- SPDX-License-Identifier: MPL-2.0 -->

Le catalogue des activités de rééducation-réadaptation est une nomenclature des actes des professionnels de rééducation et réadaptation. 
Il vise à améliorer la description et la connaissance de l’activité hospitalière de ce champ d’activités. 
Il a été développé pour le programme de médicalisation des systèmes d’information (<link-previewer href="PMSI.html" text="PMSI" preview-title="PMSI - Programme de médicalisation des systèmes d’information" preview-text="Le PMSI permet de décrire de façon synthétique et standardisée l’activité médicale des établissements de santé. Il repose sur l’enregistrement de données médico-administratives normalisées dans un recueil standard d’information. Il comporte 4 « champs » : « médecine, chirurgie, obstétrique et odontologie » (MCO) « soins de suite ou de réadaptation » (SSR) « psychiatrie » sous la forme du RIM-Psy (recueil d’information médicale en psychiatrie) « hospitalisation à domicile » (HAD)" />) en soins de suites et de réadaptation (<link-previewer href="SSR.html" text="SSR" preview-title="SSR - Soins de suite et de réadaptation" preview-text="Les soins de suite et de réadaptation (SSR) désignent un dispositif qui a pour objet de prévenir ou de réduire les conséquences fonctionnelles, physiques, cognitives, psychologiques ou sociales des déficiences et des limitations de capacité des patients et de promouvoir leur réadaptation et leur réinsertion. " />). 

# Références

- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/catalogue-des-activites-de-reeducation-readaptation-cdarr) sur le site internet du ministère
